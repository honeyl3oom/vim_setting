set wrapscan
set t_Co=256
set backspace=start,indent,eol
set number
set mousehide
set hlsearch
set sw=2
set ts=2
set ai
set si
set expandtab
set smarttab
set fileencodings=utf-8,euc-kr
se ff=unix


set nocompatible
filetype off

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

Bundle 'gmarik/vundle'

Bundle 'tpope/vim-fugitive'
Bundle 'The-NERD-tree'
Bundle 'ctrlp.vim'
Bundle 'mattn/emmet-vim'
Bundle 'charset.vim'
Bundle 'AutoFenc.vim'
Bundle 'The-NERD-Commenter'
Bundle 'pathogen.vim'
Bundle 'php.vim'
Bundle 'Auto-Pairs'
Bundle 'JulesWang/css.vim'
Bundle 'cakebaker/scss-syntax.vim'
Bundle 'kchmck/vim-coffee-script'

Bundle 'git://git.wincent.com/command-t.git'

call pathogen#infect()
call pathogen#helptags()
syntax on
filetype plugin indent on

colorscheme molokai

let g:pymode_virtualenv = 1

map <C-l> :tabn<CR>
map <C-h> :tabp<CR>
map <C-n> :tabnew<CR>

autocmd CursorMoved * silent! exe printf('match IncSearch /\V\<%s\>/', expand('<cword>','/\'))

let g:ctrlp_show_hidden = 1

let g:rubycomplete_buffer_loading = 1
let g:rubycomplete_rails = 1
